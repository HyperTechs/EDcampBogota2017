<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Historical extends Model
{
    protected $table = 'historicals';

    protected $dateFormat = 'Y-m-d H:i:s.u';

    public function chapter()
    {
        return $this->belongsTo(Chapter::class);
    }
}
