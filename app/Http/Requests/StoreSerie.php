<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreSerie extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() && auth()->user()->rol->id === 1;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'min:2',
                Rule::unique('series'),
            ],
            'picture' => 'image',
            'information' => 'required|min:6',
            'tags' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'El nombre es obligatorio',
            'name.min' => 'El nombre debe tener por lo menos 2 letras',
            'name.unique' => 'Una serie con ese nombre ya ha sido registrada',
            'picture.image' => 'El archivo adjuntado no se reconoce como imagen',
            'information.required' => 'La información de la serie es obligatoria',
            'information.min' => 'La serie necesita más información',
            'trailer_url.required' => 'La URL del trailer es obligatoria',
            'tags.required' => 'Los tags son obligatorios',
        ];
    }
}
