<?php

namespace App\Http\Controllers;

use App\Models\Price;
use Illuminate\Http\Request;

class PricesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prices = Price::orderBy('actual', 'desc')->orderBy('ends_at', 'desc')->paginate();

        return view('prices.index', compact('prices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('prices.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $price = new Price($request->only(['begins_at', 'ends_at', 'price']));

        $price->begins_at = $price->begins_at->addSeconds(86399);
        $price->ends_at = $price->ends_at->addSeconds(86399);

        $price->actual = $request->get('actual') ? true : false;

        $price->save();

        return redirect()->route('admin.prices.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $price = Price::findOrFail($id);

        return view('prices.edit', compact('price'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $price = Price::findOrFail($id);

        $price->begins_at = $request->get('begins_at');
        $price->begins_at = $price->begins_at->addSeconds(86399);

        $price->ends_at = $request->get('ends_at');
        $price->ends_at = $price->ends_at->addSeconds(86399);

        $price->price = $request->get('price');
        $price->actual = $request->get('actual') ? true : false;

        $price->save();

        return redirect()->route('admin.prices.index');
    }
}
