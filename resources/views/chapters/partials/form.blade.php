<div class="Form-element{{ $errors->has('name') ? ' u-error' : '' }}">
    {!! Form::label('name', 'Nombre:') !!}
    {!! Form::text('name', null,
        [
            'placeholder' => 'Nombre del capítulo',
            'autocomplete' => 'off',
            'required',
            'maxlength' => 255,
            'class' => ''
        ])
    !!}
</div>
@if($errors->has('name'))
    <div class="Form-message  u-alert">
        <strong>{{ $errors->first('name') }}</strong>
    </div>
@endif
<div class="Form-element{{ $errors->has('chapter_url') ? ' u-error' : '' }}">
    {!! Form::label('chapter_url', 'Video:') !!}

    {!! Form::textarea('chapter_url',
        null,
        [
            'required',
            'placeholder' => 'Video',
            'maxlength' => 1024,
            'class' => ''
        ]
    ) !!}
</div>
@if($errors->has('chapter_url'))
    <div class="Form-message  u-alert">
        <strong>{{ $errors->first('chapter_url') }}</strong>
    </div>
@endif
<div class="Form-element{{ $errors->has('duration') ? ' u-error' : '' }}">
    {!! Form::label('duration', 'Duración:') !!}
    {!! Form::text('duration', null,
        [
            'required',
            'placeholder' => 'HH:mm:ss',
            'autocomplete' => 'off',
            'class' => ''
        ])
    !!}
</div>
@if($errors->has('duration'))
    <div class="Form-message  u-alert">
        <strong>{{ $errors->first('duration') }}</strong>
    </div>
@endif
<div class="Form-element{{ $errors->has('description') ? ' u-error' : '' }}">
    {!! Form::label('description', 'Descripción:') !!}

    {!! Form::textarea('description',
        null,
        [
            'required',
            'autocomplete' => 'off',
            'placeholder' => 'Descripción',
            'class' => ''
        ]
    ) !!}
</div>
@if($errors->has('description'))
    <div class="Form-message  u-alert">
        <strong>{{ $errors->first('description') }}</strong>
    </div>
@endif
<div class="Form-element  u-bg-success  u-lg-w35">
    <input type="submit" value="Guardar Cambios">
</div>
