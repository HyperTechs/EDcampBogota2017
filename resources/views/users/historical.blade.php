@extends('layouts.master')

@section('title')
    Series
@endsection

@section('header')
    @include('partials.header')
    @include('partials.user_menu')
@endsection

@section('content')
    <main class="History u-afterFixed">
        <h1>Historial</h1>

        @if($historicals->isEmpty())
            <header class="Tables-title">
                <h2>0 resultados</h2>
            </header>
        @else
            <div class="Table-container">
                <table class="pure-table pure-table-horizontal">
                    <thead>
                    <tr>
                        <th>Capítulo</th>
                        <th>Serie</th>
                        <th>Fecha</th>
                    </tr>
                    </thead>
                    @foreach($historicals as $historical)
                        <tbody>
                        <tr>
                            <td>{{ $historical->chapter->name }}</td>
                            <td>{{ $historical->chapter->serie->name }}</td>
                            <td>
                                {{ $historical->updated_at->format('Y-m-d H:i') }}
                            </td>
                        </tr>
                        </tbody>
                    @endforeach
                </table>
            </div>
        @endif
        <div class="text-center">
            {!! $historicals->links() !!}
        </div>
    </main>
@endsection
