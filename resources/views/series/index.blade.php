@extends('layouts.master')

@section('title')
    Series
@endsection

@section('header')
    @include('partials.header')
    @include('partials.user_menu')
@endsection

@section('content')
    <main class="Series u-afterFixed">
        <header class="Tables-title">
            <h2>Series</h2>
            <a href="{{ route('admin.series.create') }}" class="u-button  u-bg-alert  u-white">Añadir serie</a>
        </header>
        @if($series->isEmpty())
            <header class="u-title">
                <h2>0 resultados</h2>
            </header>
        @else
            <div class="Table-container">
                <table class="pure-table pure-table-horizontal">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Serie</th>
                            <th>Tags</th>
                            <th colspan="3">Acciones</th>
                        </tr>
                    </thead>
                    @foreach($series as $serie)
                        <tbody>
                            <tr>
                                <td>{{ $serie->id }}</td>
                                <td>{{ $serie->name }}</td>
                                <td>
                                    @foreach($serie->tags as $tag)
                                        <a class="Label  u-small" href="{{ route('admin.series.index') }}?tag={{ $tag->name }}">{{ $tag->name }}</a>
                                    @endforeach
                                </td>
                                <td>
                                    <a href="{{ route('admin.chapters.create', $serie) }}">Añadir Capítulo</a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.series.edit', $serie) }}">Editar</a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.series.show', $serie) }}">Ver más</a>
                                </td>
                            </tr>
                        </tbody>
                    @endforeach
                </table>
            </div>
        @endif
        <div class="text-center">
            @if(Request::has('search'))
                {!! $series->appends('search', Request::get('search'))->links() !!}
            @elseif(Request::has('tag'))
                {!! $series->appends('tag', Request::get('tag'))->links() !!}
            @else
                {!! $series->links() !!}
            @endif
        </div>
    </main>
@endsection
