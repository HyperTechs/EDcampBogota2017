<div class="Form-element{{ $errors->has('begins_at') ? ' has-error' : '' }}">
    {!! Form::label('begins_at', 'Desde:') !!}
    {!! Form::date('begins_at',
        $price->begins_at ? $price->begins_at->format('Y-m-d') : date('Y-m-d'),
        [
            'placeholder' => 'yyyy/mm/dd',
            'required',
            'class' => 'form-control'
        ])
    !!}
    @if($errors->has('begins_at'))
        <div class="Form-message  u-alert">
            <strong>{{ $errors->first('begins_at') }}</strong>
        </div>
    @endif
</div>
<div class="Form-element{{ $errors->has('ends_at') ? ' has-error' : '' }}">
    {!! Form::label('ends_at', 'Hasta:') !!}
    {!! Form::date('ends_at',
        $price->ends_at ? $price->ends_at->format('Y-m-d') : date('Y-m-d'),
        [
            'placeholder' => 'yyyy/mm/dd',
            'required',
            'class' => 'form-control'
        ])
    !!}
    @if($errors->has('ends_at'))
        <div class="Form-message  u-alert">
            <strong>{{ $errors->first('ends_at') }}</strong>
        </div>
    @endif
</div>
<div class="Form-element{{ $errors->has('price') ? ' has-error' : '' }}">
    {!! Form::label('price', 'Precio:') !!}
    {!! Form::number('price', null,
        [
            'placeholder' => 'Precio',
            'required',
            'step' => 0.01,
            'min' => 0.01,
            'max' => 99999.99,
            'class' => 'form-control'
        ])
    !!}
    @if($errors->has('price'))
        <div class="Form-message  u-alert">
            <strong>{{ $errors->first('price') }}</strong>
        </div>
    @endif
</div>

<div class="Form-element{{ $errors->has('actual') ? ' has-error' : '' }}">
    {!! Form::label('actual', 'Actual:') !!}
    {!! Form::checkbox('actual', null) !!}

    @if($errors->has('actual'))
        <div class="Form-message  u-alert">
            <strong>{{ $errors->first('actual') }}</strong>
        </div>
    @endif
</div>
<div class="Form-element  u-bg-success  u-lg-w35">
    <input type="submit" value="Guardar Cambios">
</div>
