-- CREA LA TABLA SUSCRIPCIONES
CREATE TABLE subscriptions (
  id SERIAL NOT NULL,
  invoice_id INT NOT NULL,
  user_id INT NOT NULL,
  begins_at DATE NOT NULL DEFAULT now(),
  ends_at DATE NOT NULL,
  actual BOOLEAN NOT NULL DEFAULT true,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  updated_at TIMESTAMP,
  CONSTRAINT subscriptions_id_pk PRIMARY KEY (id),
  CONSTRAINT subscriptions_invoice_id_fk FOREIGN KEY (invoice_id) REFERENCES invoices (id) ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT subscriptions_user_id_fk FOREIGN KEY (user_id) REFERENCES users (id) ON UPDATE RESTRICT ON DELETE RESTRICT
);

COMMENT ON TABLE subscriptions IS 'Suscripciones de los usuarios al sistema';
